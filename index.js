const express = require("express"); // Tương tự : import express from "express";
const serverless = require("serverless-http");
const path = require("path");

// Khởi tạo Express App
const app = express();

// Khai báo ứng dụng sẽ chạy trên cổng 8000
const port = 8000;

//Khai báo mongoose
var mongoose = require('mongoose');

//khai báo routers
const courseRouter = require("./app/routers/course.router");

// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());

//kết nối mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course_365")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));
//khai báo các model
const courseModel = require("./app/models/course.model");

//load các thành phần tĩnh như ảnh, css
app.use(express.static(__dirname + "/views"))

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/index.html"))
})

app.get("/admin", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/courseAdmin.html"))
})

app.use("/api/v1/courses", courseRouter);

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})

module.exports = app;
module.exports.handler = serverless(app);