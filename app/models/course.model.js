//b1: khai báo thư viện mongo
const mongoose = require("mongoose")

//b2: khai báo class Schema
const Schema = mongoose.Schema

//b3: khởi tạo Shema với các thuộc tính của collection
const courseSchema = new Schema ({
    _id: mongoose.Types.ObjectId, //có thể khai báo hoặc không, vì mongoose thường tự sinh
    courseCode: {
        type: String,
        unique: true,
        required: true,
    },
    courseName: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    discountPrice: {
        type: Number,
        required: true,
    },
    duration: {
        type: String,
        required: true,
    },
    level: {
        type: String,
        required: true,
    },
    coverImage: {
        type: String,
        required: true,
    },
    teacherName: {
        type: String,
        required: true,
    },
    teacherPhoto: {
        type: String,
        required: true,
    },
    isPopular: {
        type: Boolean,
        default: true,
    },
    isTrending: {
        type: Boolean,
        default: false,
    },
});

//b4:biên dịch shema thành model
module.exports = mongoose.model("course", courseSchema);