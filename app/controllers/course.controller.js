//import thư viện mongoose
const mongoose = require('mongoose');
//import course model
const courseModel = require('../models/course.model');

const createCourse = (req, res) => {
    //B1: thu thập dữ liệu
    const { courseCode, courseName, price, discountPrice, duration, level, coverImage, teacherName, teacherPhoto, isPopular, isTrending } = req.body;

    //B2: kiểm tra dữ liệu
    if (!courseCode) {
        return res.status(400).json({
            status:"Bad request",
            message:"courseCode is required!"
        })
    }
    if (!courseName) {
        return res.status(400).json({
            status:"Bad request",
            message:"courseName is required!"
        })
    }
    if (!price) {
        return res.status(400).json({
            status:"Bad request",
            message:"price is required!"
        })
    }
    if (!Number.isInteger(price) || price<0) {
        return res.status(400).json({
            status:"Bad request",
            message:"price is invalid!"
        })
    }
    if (!discountPrice) {
        return res.status(400).json({
            status:"Bad request",
            message:"discountPrice is required!"
        })
    }
    if (!Number.isInteger(discountPrice) || discountPrice<0) {
        return res.status(400).json({
            status:"Bad request",
            message:"discountPrice is invalid!"
        })
    }
    if (!duration) {
        return res.status(400).json({
            status:"Bad request",
            message:"duration is required!"
        })
    }
    if (!level) {
        return res.status(400).json({
            status:"Bad request",
            message:"level is required!"
        })
    }
    if (!coverImage) {
        return res.status(400).json({
            status:"Bad request",
            message:"coverImage is required!"
        })
    }
    if (!teacherName) {
        return res.status(400).json({
            status:"Bad request",
            message:"teacherName is required!"
        })
    }
    if (!teacherPhoto) {
        return res.status(400).json({
            status:"Bad request",
            message:"teacherPhoto is required!"
        })
    }

    //B3: thực hiện thao tác model
    let newCourse = {
        _id: new mongoose.Types.ObjectId(),
        courseCode,
        courseName,
        price,
        discountPrice,
        duration,
        level,
        coverImage,
        teacherName,
        teacherPhoto,
        isPopular,
        isTrending
    }

    courseModel.create(newCourse)
                .then((data) => {
                    return res.status(201).json({
                        status:"Create new course sucessfully",
                        data
                    })
                })
                .catch((error) => {
                    if (error.name === 'MongoServerError' && error.code === 11000) {
                        // Duplicate courseCode
                        return res.status(500).json({
                            status:"Internal Server Error",
                            message: "courseCode already exist",
                        });
                      }
                    else { //other error
                        return res.status(500).json({
                            status:"Internal Server Error",
                            message:error.message
                        })
                    }
                })
}

const getAllCourse = (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    courseModel.find()
                .then((data) => {
                    if (data && data.length > 0) {
                        return res.status(200).json({
                            status:"Get all courses sucessfully",
                            data
                        })
                    } else {
                        return res.status(404).json({
                            status:"Not found any course",
                            data
                        })
                    }

                })
                .catch((error) => {
                    return res.status(500).json({
                        status:"Internal Server Error",
                        message:error.message
                    })
                })
}

const getCourseById = (req, res) => {
    //B1: thu thập dữ liệu
    var courseId = req.params.courseId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    courseModel.findById(courseId)
                .then((data) => {
                    if (data) {
                        return res.status(200).json({
                            status:"Get course by id sucessfully",
                            data
                        })
                    } else {
                        return res.status(404).json({
                            status:"Not found any course",
                            data
                        })
                    }

                })
                .catch((error) => {
                    return res.status(500).json({
                        status:"Internal Server Error",
                        message:error.message
                    })
                })
}

const updateCourse = (req, res) => {
    //B1: thu thập dữ liệu
    var courseId = req.params.courseId;
    const { courseCode, courseName, price, discountPrice, duration, level, coverImage, teacherName, teacherPhoto, isPopular, isTrending } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    if (!courseCode) {
        return res.status(400).json({
            status:"Bad request",
            message:"courseCode is required!"
        })
    }
    if (!courseName) {
        return res.status(400).json({
            status:"Bad request",
            message:"courseName is required!"
        })
    }
    if (!price) {
        return res.status(400).json({
            status:"Bad request",
            message:"price is required!"
        })
    }
    if (!Number.isInteger(price) || price<0) {
        return res.status(400).json({
            status:"Bad request",
            message:"price is invalid!"
        })
    }
    if (!discountPrice) {
        return res.status(400).json({
            status:"Bad request",
            message:"discountPrice is required!"
        })
    }
    if (!Number.isInteger(discountPrice) || discountPrice<0) {
        return res.status(400).json({
            status:"Bad request",
            message:"discountPrice is invalid!"
        })
    }
    if (!duration) {
        return res.status(400).json({
            status:"Bad request",
            message:"duration is required!"
        })
    }
    if (!level) {
        return res.status(400).json({
            status:"Bad request",
            message:"level is required!"
        })
    }
    if (!coverImage) {
        return res.status(400).json({
            status:"Bad request",
            message:"coverImage is required!"
        })
    }
    if (!teacherName) {
        return res.status(400).json({
            status:"Bad request",
            message:"teacherName is required!"
        })
    }
    if (!teacherPhoto) {
        return res.status(400).json({
            status:"Bad request",
            message:"teacherPhoto is required!"
        })
    }

    //B3: thực thi model
    let updateCourse = {
        courseCode,
        courseName,
        price,
        discountPrice,
        duration,
        level,
        coverImage,
        teacherName,
        teacherPhoto,
        isPopular,
        isTrending
    }

    courseModel.findByIdAndUpdate(courseId, updateCourse)
                .then((data) => {
                    if (data) {
                        return res.status(200).json({
                            status:"Update course sucessfully",
                            data
                        })
                    } else {
                        return res.status(404).json({
                            status:"Not found any course",
                            data
                        })
                    }

                })
                .catch((error) => {
                    if (error.name === 'MongoServerError' && error.code === 11000) {
                        // Duplicate courseCode
                        return res.status(500).json({
                            status:"Internal Server Error",
                            message: "courseCode already exist",
                        });
                      }
                    else { //other error
                        return res.status(500).json({
                            status:"Internal Server Error",
                            message:error.message
                        })
                    }
                })
    
}

const deleteCourse = (req, res) => {
    //B1: Thu thập dữ liệu
    var courseId = req.params.courseId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    courseModel.findByIdAndDelete(courseId)
                .then((data) => {
                    if (data) {
                        return res.status(200).json({
                            status:"Delete course sucessfully",
                            data
                        })
                    } else {
                        return res.status(404).json({
                            status:"Not found any course",
                            data
                        })
                    }

                })
                .catch((error) => {
                    return res.status(500).json({
                        status:"Internal Server Error",
                        message:error.message
                    })
                })
}

module.exports = { createCourse, getAllCourse, getCourseById, updateCourse, deleteCourse }