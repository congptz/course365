const express = require("express");

//const drinkMiddleware = require("../middlewares/drink.middleware");
const courseController = require("../controllers/course.controller");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request method: ", req.method);

    next();
});

router.get("/", /*courseMiddleware.getAllCourseMiddleware,*/ courseController.getAllCourse)

router.post("/", courseController.createCourse);

router.get("/:courseId", courseController.getCourseById);

router.put("/:courseId", courseController.updateCourse);

router.delete("/:courseId", courseController.deleteCourse);

module.exports = router;